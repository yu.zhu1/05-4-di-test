package com.twuc.webApp.domain.mazeRender;

import java.awt.*;

/**
 * 使用指定的方式渲染一整块指定的区域。
 */
public interface AreaRender {

    /**
     * 使用指定的方式渲染 {@code graphics} 中的一整块区域。
     *
     * @param graphics 渲染目标。
     * @param fullArea 渲染区域（像素）。
     */
    void render(Graphics2D graphics, Rectangle fullArea);
}
